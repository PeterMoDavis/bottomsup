package com.example.bottomsup.viewmodel

import com.example.bottomsup.model.response.DrinkDetailsDTO

data class DrinkDetailState (
    val isLoading: Boolean = false,
    val drinkDetails: List<DrinkDetailsDTO.Drink> = emptyList()
)