package com.example.bottomsup.viewmodel

import com.example.bottomsup.model.response.CategoryDrinksDTO

data class DrinksState(
    val isLoading: Boolean = false,
    val drinks: List<CategoryDrinksDTO.Drink> = emptyList()
)