package com.example.bottomsup.viewmodel

import com.example.bottomsup.model.response.CategoryDTO

data class CategoryState(
    val isLoading: Boolean = false,
    val categories: List<CategoryDTO.CategoryItem> = emptyList()
)