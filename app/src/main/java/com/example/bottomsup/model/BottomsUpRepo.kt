package com.example.bottomsup.model

import com.example.bottomsup.model.remote.BottomsUpService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object BottomsUpRepo {

    private val bottomsUpService by lazy {BottomsUpService.getInstance()}

    suspend fun getCategories() = withContext(Dispatchers.IO) {
        bottomsUpService.getCategories()
    }

    suspend fun getCategoryDrinks(category: String) = withContext(Dispatchers.IO){
        bottomsUpService.getCategoryDrinks(category).drinks
    }

    suspend fun getDrinkDetails(drink: Int) = withContext(Dispatchers.IO){
        bottomsUpService.getDrinkDetails(drink).drinks
    }
}