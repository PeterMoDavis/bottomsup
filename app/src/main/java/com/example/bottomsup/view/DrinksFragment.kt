package com.example.bottomsup.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.bottomsup.R
import com.example.bottomsup.databinding.FragmentDrinksBinding
import com.example.bottomsup.viewmodel.DrinksViewModel

class DrinksFragment : Fragment(R.layout.fragment_drinks) {
    private var _binding : FragmentDrinksBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<DrinksFragmentArgs>()
    private val drinksViewModel by viewModels<DrinksViewModel>()
    private val drinksAdapter by lazy {DrinksAdapter()}

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinksBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drinksViewModel.getDrinkState(args.getCategory)
        drinksViewModel.state.observe(viewLifecycleOwner){  drinkState ->
                binding.drinkList.adapter = drinksAdapter.apply {
                    addDrinks(drinkState.drinks)
                    addItemClickListener { drinks: String ->
                            findNavController().navigate(DrinksFragmentDirections.actionDrinksFragmentToDrinkDetailFragment(drinks.toInt()))
                    }
                }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}